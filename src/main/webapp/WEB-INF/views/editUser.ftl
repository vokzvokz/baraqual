<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Create User Page</title>
</head>
<body>
    <form name = "user" action = "/updateUser" method="post" accept-charset="UTF-8">
        <p>Username</p>
        <input title="Nickname" type="text" name="nickname" value="${user.nickname}">
        <br>
        <p>Name</p>
        <input title="Name" type="text" name="name" value="${user.name}">
        <br>
        <p>Age</p>
        <input title="Age" type="text" name="age" value="${user.age}">
        <br>
        <input type="submit" value="OK">
    </form>
</body>
</html>