<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>User Info</title>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
</head>
<body>
<h1>User</h1>
    <table>
        <tr>
            <td>Username</td>
            <td>${user.nickname}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>${user.name}</td>
        </tr>
    </table>
<br>
<div class="table">
    <div class="head-row">
        <div class="cell" style="width: 100%">Purchases</div>
    </div>
    <#list purchases as purchase>
        <div class="row">
            <div class="cell">${purchase.user}</div>
            <div class="cell">${purchase.subscription}</div>
            <div class="cell">
                ${purchase.delivered?c}
            </div>
        </div>
    </#list>
</div>

<a href = "/users">back</a>
</body>
</html>