<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home Page</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#buttonDemo1").click(function() {
                $.ajax({
                    type:'GET',
                    url:'ajax/demo1',
                    success: function (result) {
                        $('#result1').html(result);
                    }
                });
            });

            $("#buttonDemo2").click(function() {
                var fullName = $('#fullName').val();
                $.ajax({
                    type:'GET',
                    url:'ajax/demo2/'+fullName,
                    success: function (result) {
                        $('#result2').html(result);
                    }
                });
            });
        });
    </script>
</head>
<body>
    <fieldset>
        <legend>Demo 1</legend>
        <input type="button" value="Demo 1" id="buttonDemo1">
        <br>
        <span id="result1"></span>
    </fieldset>

    <fieldset>
        <legend>Demo 2</legend>
        Full Name <input type="text" id="fullName">
        <input type="button" value="Demo 2" id="buttonDemo2">
        <br>
        <span id="result2"></span>
    </fieldset>
</body>
</html>

