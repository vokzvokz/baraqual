<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>BARAQUAL</title>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            <#list nonDeliveredProducts as product>
            $("#del_${product.id}_").click(function () {
                var verify = confirm("Are you sure we delivered it to you?");
                if(verify){
                    $.ajax({
                        type: 'GET',
                        url: '/deliver/' + ${product.id},
                        success: function (result) {

                            //$('#nonDeniveredList').html(result);
                            window.location.reload();
                        },
                        error: function () {
                            alert("fail");
                        }

                    });
                }
            });
            </#list>
            $("#openshop").click(function (){
                var verify = confirm("Are you sure you want to create your own shop on our platform? There cannot be any undelivered to you items. If you choose OK, you will need to sign in again.");
                if(verify === false){
                    document.location.href = "/homepage";
                } else {
                    $.ajax({
                        type: 'GET',
                        url: '/addShop/${user.nickname}',
                        success: function (result) {
                            if(result == 'OK'){
                                document.location.href = "/addShop";
                            }
                        },
                        error: function () {
                            alert("Something wrong.");
                            document.location.href = "/homepage";
                        }
                    });
                }
            });
        });

    </script>
</head>
<body>
<header>
    <h1><a href="/homepage">BARAQUAL</a></h1>
    <ul class="main-menu">
        <li><a href="/shops">Our shops</a></li>
        <li><a href="/products">Find product</a></li>
        <li><a href="/login">Sign in as other user</a></li>
        <li><a href="/exit">Exit</a> </li>
        <li><div id = "openshop">Open your own shop</div></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>
<div style="padding: 10px"></div>
<p>Hello, ${user.name}!</p>
<p>Here is products we sent to you</p>
<div class="table">
    <div id="nonDeniveredList">
        <#list nonDeliveredProducts as product>
            <div class="row"><div class="cell" style="width: 400px"> ${product.subscription} </div>
                <div id="del_${product.id}_" class="but delivered-button">I've got it!</div>
            </div>
        </#list>
    </div>
</div>


</body>
</html>