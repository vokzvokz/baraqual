<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Create User Page</title>
    <link rel="stylesheet" type="text/css" href="/style/form.css">
</head>
<body>
<form name="user" action="/addUser" method="post" class="form-signin">
    <input title="Name" type="text" name="name" placeholder="NAME" class="input">
    <br>
    <input title="Nickname" type="text" name="nickname" placeholder="USERNAME" class="input">
    <br>
    <input title="Password" type="password" name="password" placeholder="PASSWORD" class="input">
    <br>
    <input type="submit" value="OK" class="submit">
</form>
</body>
</html>