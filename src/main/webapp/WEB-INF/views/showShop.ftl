<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>User Info</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
    <link rel="stylesheet" type="text/css" href="/style/shop.css">

</head>
<body>
<header>
    <#--<div class="search-input">-->
    <#--<input type="text" id="searchField" placeholder="SEARCH HERE">-->
    <#--</div>-->
    <h1><a href="/homepage">BARAQUAL</a></h1>
    <ul class="main-menu">
        <li><a href="/shops">Our shops</a></li>
        <li><a href="/products">Find product</a></li>
        <ii><a href="/login">Sign in as other user</a></ii>
        <li><a href="/exit">Exit</a> </li>
        <li><div id = "openshop">Open your own shop</div></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>
<img src="${shop.logoPath}" class="logo" style="margin-right: 10px">
<h1>Shop</h1>
<table class="info-table">
    <tr>
        <td>Name</td>
        <td>${shop.name}</td>
    </tr>
    <tr>
        <td>Approved quality</td>
        <td>${shop.prove}</td>
    </tr>
    <tr>
        <td>Items sold</td>
        <td>${shop.sold}</td>
    </tr>
    <tr>
        <td>About ${shop.name}</td>
        <td>${shop.selfDescription}</td>
    </tr>
</table>
<h2>Продукция:</h2>
<div class="table" style="margin-left: 10px;">
    <div class="head-row">
        <div class="cell">Name</div>
        <div class="cell">Price</div>
        <div class="cell">Subscription</div>
        <div class="cell">Added at</div>
    </div>
    <#list products as product>
        <a href="/product/${product.id}">
        <li class="row">
            <div class="cell">${product.name}</div>
            <div class="cell">${product.price}</div>
            <div class="cell">${product.subscription}</div>
            <div class="cell">${product.addDate}</div>
            <#--<td>-->
                <#--<input type="button" value="Купить" class="buy-button but" id="buy/${product.id}">-->
            <#--</td>-->
        </li>
        </a>
    </#list>
</div>
</body>
</html>