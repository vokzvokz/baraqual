<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BARAQUAL</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">

    <!-- Custom styles for this template -->
</head>
<body>
<header>
    <h1><a href="/homepage">${shop.name?upper_case}</a></h1>
    <ul class="main-menu">
        <li><a href="/login">Sign in as other user</a></li>
        <li><a href="/exit">Exit</a> </li>
        <li><a href="/addProduct"> Add product</a></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>
<img src="${shop.logoPath}" alt="LOGO" class="logo" style="margin-right: 10px">
<p>Hello, ${user.nickname}! Your shop is ${shop.name} <br>
Here is your products:</p>
<div class="table">
    <div class="head-row">
        <div class="cell">ID</div>
        <div class="cell">Name</div>
        <div class="cell">Price</div>
        <div class="cell">Description</div>
        <div class="cell">Added at</div>
        <div class="cell">Already sold</div>
    </div>
    <#list products as product>
        <div class="row">
            <div class="cell">${product.id}</div>
            <div class="cell">${product.name}</div>
            <div class="cell">${product.price}</div>
            <div class="cell">${product.subscription}</div>
            <div class="cell">${product.addDate}</div>
            <div class="cell">${product.sold}</div>
        </div>
    </#list>
</div>
<br>
<#--<a href = "/products/${shop.username}">Наши товары</a>-->
</body>
</html>