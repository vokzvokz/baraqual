<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="/style/form.css">
</head>
<body>
<form name="LoginForm" action="/login" method="post" class="form-signin">
    <input type="text" name="username" placeholder="USERNAME" class="input" />
    <br>
    <input type="password" name="password" placeholder="PASSWORD" class="input" />
    <br>
    <input type="submit" value="LOGIN" class="submit" />
</form>
</body>
</html>