<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BARAQUAL</title>
    <link rel="stylesheet" type="text/css" href="/style/form.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#submit-button").click(function() {
                var verify = confirm("Are you sure you want to open new shop on our platform? " +
                    "After form filling you will need to sign in again.");
            });
        });
    </script>
</head>
<body>
<form name="user" enctype="multipart/form-data" action="/addShop" method="post" class="form-signin">
    <input title="Name" type="text" name="name" PLACEHOLDER="NAME" class="input">
    <br>
    <input title="Selfdescription" type="text" name="selfDescription" placeholder="DESCROPTION" class="input">
    <br>
    <p>LOGO: </p>
    <input type="file" name="file" accept="image/*">
    <br>
    <input id="submit-button" type="submit" value="OK" class="submit">
</form>
</body>
</html>