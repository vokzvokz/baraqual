<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
</head>
<body>
<header>
    <#--<div class="search-input">-->
    <#--<input type="text" id="searchField" placeholder="SEARCH HERE">-->
    <#--</div>-->
    <h1>BARAQUAL</h1>
    <ul class="main-menu">
        <li><a href="/shops">Our shops</a></li>
        <li><a href="/products">Find product</a></li>
        <ii><a href="/login">Sign in as other user</a></ii>
        <li><a href="/exit">Exit</a> </li>
        <li><a href="/users">Users</a></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>

    <div class="table">
        <div class="head-row">
            <div class="cell">Username</div>
            <div class="cell">Name</div>
            <div class="cell">Delete?</div>
        </div>
        <#list users as user>
            <div class="row">
                <div class="cell"><a href = "/user/${user.nickname}">${user.nickname}</a> </div>
                <div class="cell">${user.name}</div>
                <div class="cell"><a class="but" href = "/delete/${user.nickname}">Delete</a></div>
            </div>
        </#list>
    </div>
</body>
</html>