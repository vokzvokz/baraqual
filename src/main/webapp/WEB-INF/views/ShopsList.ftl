<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Our Shops</title>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>
<header>
    <#--<div class="search-input">-->
        <#--<input type="text" id="searchField" placeholder="SEARCH HERE">-->
    <#--</div>-->
    <h1><a href="/homepage">BARAQUAL</a></h1>
    <ul class="main-menu">
        <li><a href="/shops">Our shops</a></li>
        <li><a href="/products">Find product</a></li>
        <ii><a href="/login">Sign in as other user</a></ii>
        <li><a href="/exit">Exit</a> </li>
        <li><div id = "openshop">Open your own shop</div></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>
<div style="padding: 10px"></div>
<div class="table">
    <div class="head-row">
        <div class="cell">Name</div>
        <div class="cell">Approved quality</div>
        <div class="cell">Items sold</div>
    </div>
    <div>
        <#list shops as shop>
            <a href="/shop/${shop.owner}">
                <div class="row">
                    <div class="cell">${shop.name}</div>
                    <div class="cell">${shop.prove}</div>
                    <div class="cell">${shop.sold}</div>
                </div>
            </a>
        </#list>
    </div>
</div>
</body>
</html>