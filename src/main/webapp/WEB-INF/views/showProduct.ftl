<#--<#ftl encoding="windows-1251"/>-->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>User Info</title>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#buyButton").click(function () {
                var productId = $('#idVal').val();
                //alert(productId);
                $.ajax({

                    type: 'GET',
                    url: 'buy/' + productId,
                    success: function (result) {
                        alert("Item " + productId + " sold!");
                        //$('#result').html(result);
                    },
                    error: function () {
                        alert("fail");
                    }

                });
            });
        });
    </script>
</head>
<body>
<header>
    <h1><a href="/homepage">BARAQUAL</a></h1>
    <ul class="main-menu">
        <li><a href="/shops">Our shops</a></li>
        <li><a href="/products">Find product</a></li>
        <li><a href="/login">Sign in as other user</a></li>
        <li><a href="/exit">Exit</a> </li>
        <li><div id = "openshop">Open your own shop</div></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>
<input id="idVal" type="text" value="${product.id}" style="visibility: hidden">
<img src="${product.photoPath}" class="logo">
<div class="table" style="margin-left: 10px">
    <div class="row">
        <div class="cell">Name</div>
        <div class="cell">${product.name}</div>
    </div>
    <div class="row">
        <div class="cell">Price</div>
        <div class="cell">${product.price}</div>
    </div>
    <div class="row">
        <div class="cell">Already Sold</div>
        <div class="cell">${product.sold}</div>
    </div>
    <div class="row">
        <div class="cell">Description</div>
        <div class="cell">${product.subscription}</div>
    </div>
    <div class="row">
        <div class="cell">Added at</div>
        <div class="cell">${product.addDate}</div>
    </div>
</div>
<div class="buy-button but" id="buyButton" style="width: 150px">Buy</div>
<#--<fieldset>-->
<#--<legend>BUY</legend>-->
<#--<input type="button" value="BUY" id="buyButton">-->
<#--<br>-->
<#--<span id="result"></span>-->
<#--</fieldset>-->
<br>
<a href="/shop/${product.shop}"><div class="but" style="width: 150px; margin: 5px;">To ${product.shop}</div></a>
</body>
</html>