<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Products</title>
    <link rel="stylesheet" type="text/css" href="/style/mystyle.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#searchField").keyup(function() {
                var mask = $('#searchField').val();
                if(mask == false) mask = "<>";
                // alert(mask);
                $.ajax({
                    type: 'GET',
                    url: 'find/' + mask,
                    success: function (result) {
                        $('#productsList').html(result);
                    },
                    error: function () {
                        alert("fail");
                    }

                });
            });
        });
    </script>
</head>
<body>
<header>
    <div class="search-input">
        <input type="text" id="searchField" placeholder="SEARCH HERE">
    </div>
    <h1><a href="/homepage">BARAQUAL</a></h1>
    <ul class="main-menu">
        <li><a href="/shops">Our shops</a></li>
        <li><a href="/products">Find product</a></li>
        <ii><a href="/login">Sign in as other user</a></ii>
        <li><a href="/exit">Exit</a> </li>
        <li><div id = "openshop">Open your own shop</div></li>
        <#--Перед переходом по ссылке проверить наличиие недоставленных товаров-->
    </ul>
</header>
<div style="padding: 10px"></div>
<div class="table">
    <div class="head-row">
        <div class="cell">Picture</div>
        <div class="cell">Name</div>
        <div class="cell">Shop</div>
        <div class="cell">Price</div>
        <div class="cell">Already sold</div>
    </div>
    <div id="productsList">
    <#list products as product>
    <a href="/product/${product.id}">
    <div class="row">
        <div class="cell"><img src="${product.photoPath}" class="logo-small"></div>
        <div class="cell">${product.name}</div>
        <div class="cell">${product.shop}</div>
        <div class="cell">${product.price}</div>
        <div class="cell">${product.sold}</div>
    </div>
    </a>
    </#list>
    </div>
</div>
</body>
</html>