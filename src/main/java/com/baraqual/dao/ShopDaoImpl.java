package com.baraqual.dao;

import com.baraqual.entity.Product;
import com.baraqual.entity.Shop;
import com.baraqual.mapper.ProductMapper;
import com.baraqual.mapper.ShopMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ShopDaoImpl implements ShopDao {
    public JdbcTemplate jdbcTemplate;

    @Autowired
    public ShopDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Shop> findAll(){
        String sql = "SELECT * FROM data.shop";
        return jdbcTemplate.query(sql, new ShopMapper());
    }

    @Override
    public List<Product> findAllProducts(String owner) {
        String sql = "SELECT * FROM data.product " +
                "where data.product.shop=?";
        return jdbcTemplate.query(sql, new ProductMapper(), owner);
    }

    @Override
    public void save(Shop shop) {
        String sql = "INSERT INTO data.shop (owner, name, logopath, " +
                "isapproved, sold, selfdescription) VALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, shop.getOwner(), shop.getName(), shop.getLogoPath(),
                shop.isApproved(), shop.getSold(), shop.getSelfDescription());
    }

    @Override
    public Shop getByOwner(String owner) {
        String sql = "SELECT * FROM data.shop where owner = ?";
        return jdbcTemplate.queryForObject(sql, new ShopMapper(), owner);
    }

    @Override
    public Shop getById(int id) {
        String sql = "SELECT * FROM data.shop WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new ShopMapper(), id);
    }

    @Override
    public void delete(String owner) {

        String sql="DELETE FROM data.shop where owner=?";
        jdbcTemplate.update(sql, owner);
        sql = "DELETE FROM data.product where owner = ?";
        jdbcTemplate.update(sql, owner);
    }

    @Override
    public void update(Shop shop) {
//        String sql = "UPDATE data.user set name =?, age=? where nickname = ?";
//        jdbcTemplate.update(sql, user.getName(), user.getAge(), user.getNickname());
        String sql = "UPDATE data.shop set name=?, logopath=?, " +
                "isapproved=?, sold=?, selfdescription=? where owner = ?";
        jdbcTemplate.update(sql, shop.getName(), shop.getLogoPath(),
                shop.isApproved(), shop.getSold(), shop.getSelfDescription(), shop.getOwner());
    }
}
