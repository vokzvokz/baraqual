package com.baraqual.dao;

import com.baraqual.entity.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();
    List<String> getUserRoles(String nickname);
    void setRole(String role, String nickname);
    void save(User user);
    User getByNickname(String nickname);
    void delete(String nickname);
    void update(User user);
}
