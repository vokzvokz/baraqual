package com.baraqual.dao;

import com.baraqual.entity.User;
import com.baraqual.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    public JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        String sql = "SELECT * FROM data.user";
        return jdbcTemplate.query(sql, new UserMapper());
    }

    @Override
    public List<String> getUserRoles(String nickname) {
        String  sql = "SELECT role FROM data.user WHERE nickname = ?";
        List<String> roles = jdbcTemplate.queryForList(sql, String.class, nickname);
        return roles;
    }

    @Override
    public void setRole(String role, String nickname){
        String sql = "UPDATE data.user SET role = ? WHERE nickname = ?";
        jdbcTemplate.update(sql, role, nickname);
    }

    @Override
    public void save(User user) {
        String sql = "INSERT INTO data.user (nickname, password, name) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, user.getNickname(), user.getPassword(), user.getName());
    }

    @Override
    public User getByNickname(String nickname) {
        String sql = "SELECT * FROM data.user WHERE data.user.nickname=?";
        return jdbcTemplate.queryForObject(sql, new UserMapper(), nickname);
    }

    @Override
    public void delete(String nickname) {

        String sql="DELETE FROM data.user where nickname=?";
        jdbcTemplate.update(sql, nickname);
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE data.user set name =? where nickname = ?";
        jdbcTemplate.update(sql, user.getName(), user.getNickname());
    }

    private class UserRoleMapper implements RowMapper<String> {

        @Override
        public String mapRow(ResultSet resultSet, int i) throws SQLException {
            String string = resultSet.getString("user_role");
            return string;
        }
    }

}
