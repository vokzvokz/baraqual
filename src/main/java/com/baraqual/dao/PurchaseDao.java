package com.baraqual.dao;

import com.baraqual.entity.Purchase;

import java.util.List;

public interface PurchaseDao {
    void createPurchase(String user, int product);
    List<Purchase> nonDelivered(String user);
    List<Purchase> findByProduct(int product);
    List<Purchase> findByUser(String user);
    List<Purchase> findAll();
    Purchase findById(int id);
    void delivered(int id);
}
