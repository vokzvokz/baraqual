package com.baraqual.dao;

import com.baraqual.entity.Product;
import com.baraqual.entity.Purchase;
import com.baraqual.mapper.ProductMapper;
import com.baraqual.mapper.PurchaseMapper;
import com.baraqual.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class PurchaseDaoImpl implements PurchaseDao {

    @Autowired
    ProductService productService;

    public JdbcTemplate jdbcTemplate;

    @Autowired
    public PurchaseDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void createPurchase(String user, int product) {
        String sql = "INSERT INTO data.purchase (customer, product_id, subscript) values (?, ?, ?)";
        Product purchased = productService.getById(product);
        jdbcTemplate.update(sql, user, product, ("Item "+purchased.getName()+", purchased "+purchased.getAddDate()));
    }

    @Override
    public List<Purchase> nonDelivered(String user) {
        String sql="SELECT * FROM data.purchase where isdelivered = false and customer = ? ORDER BY date_of_purch";
        return jdbcTemplate.query(sql, new PurchaseMapper(), user);
    }

    @Override
    public List<Purchase> findByProduct(int product) {
        String sql = "SELECT * FROM data.purchase WHERE product_id = ?";
        return jdbcTemplate.query(sql, new PurchaseMapper(), product);
    }

    @Override
    public List<Purchase> findByUser(String user) {
        String sql = "SELECT * FROM data.purchase WHERE customer = ?";
        return jdbcTemplate.query(sql, new PurchaseMapper(), user);
    }

    @Override
    public List<Purchase> findAll() {
        String sql = "SELECT * FROM data.purchase";
        return jdbcTemplate.query(sql, new PurchaseMapper());
    }

    @Override
    public Purchase findById(int id) {
        String sql = "SELECT * FROM data.purchase WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new PurchaseMapper(), id);
    }

    @Override
    public void delivered(int id) {
        String sql="UPDATE data.purchase set isdelivered = true WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }
}
