package com.baraqual.dao;

import com.baraqual.entity.Product;
import com.baraqual.entity.Shop;

import java.util.List;

public interface ShopDao {
    List<Shop> findAll();
    List<Product> findAllProducts(String owner);
    void save(Shop shop);
    Shop getByOwner(String owner);
    Shop getById(int id);
    void delete(String owner);
    void update(Shop shop);
}
