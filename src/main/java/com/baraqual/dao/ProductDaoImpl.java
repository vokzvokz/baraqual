package com.baraqual.dao;

import com.baraqual.entity.Product;
import com.baraqual.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ProductDaoImpl implements ProductDao {

    public JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Product> findAll() {
        String sql = "SELECT * from data.product";
        return jdbcTemplate.query(sql, new ProductMapper());
    }

    @Override
    public List<Product> findByName(String name) {
        String sql = "select * from data.product where name = ?";
        return jdbcTemplate.query(sql, new ProductMapper(), name);
    }

    @Override
    public List<Product> findByMask(String mask) {
        System.out.println("in product dao with mask" + mask);
        if(mask.equals("<>")){
            System.out.println("Compared "+mask+" to <>");
            mask = "%";
        }
        mask = "%" + mask + "%";
        String sql = "SELECT * FROM data.product WHERE LOWER( name ) LIKE ?";
        return jdbcTemplate.query(sql, new ProductMapper(), mask);
    }

    @Override
    public void purchased(int id) {
        String sql = "SELECT * FROM data.product where id = ?";
        Product product = jdbcTemplate.queryForObject(sql, new ProductMapper(), id);
        product.purchased();
        update(product);
    }

    @Override
    public void save(Product product) {
        if (product.getAddDate()!=null) {
            String sql="INSERT INTO data.product (name, shop, price, rating, totalmarks, subscription, sold, adddate, photopath)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, product.getName(), product.getShop(), product.getPrice(), product.getRating(),
                    product.getTotalMarks(), product.getSubscription(), product.getSold(), product.getAddDate(), product.getPhotoPath());
        } else{
            String sql="INSERT INTO data.product (name, shop, price, rating, totalmarks, subscription, sold, adddate, photopath)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, current_date, ?)";
            jdbcTemplate.update(sql, product.getName(), product.getShop(), product.getPrice(), product.getRating(),
                    product.getTotalMarks(), product.getSubscription(), product.getSold(), product.getPhotoPath());
        }
    }

    @Override
    public Product getById(int id) {
        String sql = "SELECT * FROM data.product where id = ?";
        return jdbcTemplate.queryForObject(sql, new ProductMapper(), id);
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM data.product WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void update(Product product) {
        String sql = "UPDATE data.product set name=?, shop=?, price=?, rating=?, totalmarks=?, subscription=?," +
                "sold=?, photopath=? where id=?";
        jdbcTemplate.update(sql, product.getName(), product.getShop(), product.getPrice(), product.getRating(),
                product.getTotalMarks(), product.getSubscription(), product.getSold(), product.getPhotoPath(), product.getId());
    }
}
