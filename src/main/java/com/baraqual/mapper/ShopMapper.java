package com.baraqual.mapper;

import com.baraqual.entity.Shop;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ShopMapper implements RowMapper<Shop> {

    @Override
    public Shop mapRow(ResultSet resultSet, int i) throws SQLException {
        Shop shop = new Shop();
        shop.setId(resultSet.getInt("id"));
        shop.setName(resultSet.getString("name"));
        shop.setSelfDescription(resultSet.getString("selfdescription"));
        shop.setLogoPath(resultSet.getString("logopath"));
        shop.setSold(resultSet.getInt("sold"));
        shop.setApproved(resultSet.getBoolean("isapproved"));
        shop.setOwner(resultSet.getString("owner"));
        return shop;
    }
}
