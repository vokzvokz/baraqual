package com.baraqual.mapper;

import com.baraqual.entity.Purchase;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PurchaseMapper implements RowMapper<Purchase> {

    @Override
    public Purchase mapRow(ResultSet resultSet, int i) throws SQLException {
        Purchase purchase = new Purchase();
        purchase.setId(resultSet.getInt("id"));
        purchase.setUser(resultSet.getString("customer"));
        purchase.setProduct(resultSet.getInt("product_id"));
        purchase.setDelivered(resultSet.getBoolean("isdelivered"));
        purchase.setDateOfPurchase(resultSet.getString("date_of_purch"));
        purchase.setSubscription(resultSet.getString("subscript"));
        return purchase;
    }
}
