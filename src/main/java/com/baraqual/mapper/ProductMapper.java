package com.baraqual.mapper;

import com.baraqual.entity.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet resultSet, int i) throws SQLException {
        Product product = new Product();
        product.setId(resultSet.getInt("id"));
        product.setName(resultSet.getString("name"));
        product.setShop(resultSet.getString("shop"));
        product.setPrice(resultSet.getDouble("price"));
        product.setRating(resultSet.getDouble("rating"));
        product.setTotalMarks(resultSet.getInt("totalmarks"));
        product.setSubscription(resultSet
                .getString("subscription"));
        product.setSold(resultSet.getInt("sold"));
        product.setAddDate(resultSet.getString("adddate"));
        product.setPhotoPath(resultSet.getString("photopath"));
        return product;
    }
}

//    public int id;
//    public int name;
//    public String shop;
//    public double price;
//    public double rating;
//    public int totalMarks;
//    public String subscription;
//    public int sold;
//    public String addDate;
//    public String photoPath;