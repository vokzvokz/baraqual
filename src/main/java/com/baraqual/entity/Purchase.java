package com.baraqual.entity;

public class Purchase {
    private int id;
    private String user;
    private int product;
    private boolean isDelivered;
    private String dateOfPurchase;
    private String subscription;

    public Purchase(int id, String user, int product, boolean isDelivered, String dateOfPurchase,
                    String subscription) {
        this.id = id;
        this.user = user;
        this.product = product;
        this.isDelivered = isDelivered;
        this.dateOfPurchase = dateOfPurchase;
        this.subscription = subscription;
    }

    public Purchase() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public boolean isDelivered() {
        return isDelivered;
    }

    public void setDelivered(boolean delivered) {
        isDelivered = delivered;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }
}
