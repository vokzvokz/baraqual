package com.baraqual.entity;

public class Product {
    public int id;
    public String name;
    public String shop;
    public double price;
    public double rating;
    public int totalMarks;
    public String subscription;
    public int sold;
    public String addDate;
    public String photoPath;

    public Product() {
    }

    public Product(int id, String name, String shop, double price, double rating, int totalMarks,
                   String subscription, int sold, String addDate, String photoPath) {
        this.id = id;
        this.name = name;
        this.shop = shop;
        this.price = price;
        this.rating = rating;
        this.totalMarks = totalMarks;
        this.subscription = subscription;
        this.sold = sold;
        this.addDate = addDate;
        this.photoPath = photoPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(int totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String subscription) {
        this.subscription = subscription;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public String getAddDate() {
        return addDate;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public void purchased(){
        this.sold++;
    }

//    public void recalculateRating(Mark newMark){
//        this.rating=this.rating*totalMarks+newMark.value;
//        this.totalMarks++;
//        this.rating /= this.totalMarks;
//    }
}
