package com.baraqual.entity;

public class User{

    protected String nickname;
    protected String name;
//    private String email;
    protected String password;
    protected  String role;

    public User(User user) {
        this.nickname = user.getNickname();
        this.name = user.getName();
        this.password = user.getPassword();
        this.role = user.getRole();
    }

    public User(String nickname, String name, int age, String password, String role) {
        this.nickname = nickname;
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User() {
    }

    public String getUsername() {
        return nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
