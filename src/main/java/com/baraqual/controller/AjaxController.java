package com.baraqual.controller;

import com.baraqual.config.init.CustomUserDetails;
import com.baraqual.entity.Product;
import com.baraqual.entity.Purchase;
import com.baraqual.service.ProductService;
import com.baraqual.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
public class AjaxController {

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    ProductService productService;

//    @RequestMapping(method = RequestMethod.GET)
//    public String index(){
//        return "ajax/index";
//    }

    @RequestMapping(value = "demo1", method = RequestMethod.GET)
    @ResponseBody
    public String demo1(){
        System.out.println("DDDDDDD");
        return "Demo 1";
    }

    @RequestMapping(value = "demo2/{fullName}", method = RequestMethod.GET)
    @ResponseBody
    public String demo2(@PathVariable("fullName") String fullName){
        return "Hi" + fullName;
    }

    @RequestMapping(value = "product/buy/{product}", method = RequestMethod.GET)
    @ResponseBody
    public String buyProduct(@PathVariable("product") int product, Authentication authentication){
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        String user = authUser.getNickname();
        System.out.println(user+" "+product+" sold");
        purchaseService.createPurchase(user, product);
        return "SOLD";
    }

    //@RequestMapping(value = "deliver/{purchase_id}", method = RequestMethod.GET)
    @RequestMapping(value = "deliver/{purchase_id}", method = RequestMethod.GET)
    @ResponseBody
    public String deliver(@PathVariable("purchase_id") int purchase_id, Model model, Authentication authentication){
        System.out.println("AjaxController deliver func");
        purchaseService.delivered(purchase_id);
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
//        model.addAttribute("nonDeliveredProducts", purchaseService.nonDelivered(authUser.getNickname()));
//        String result = "<#list nonDeliveredProducts as product>\n" +
//                "            <li>${product.subscription}\n" +
//                "                <div id=\"del_${product.id}_\" class=\"but delivered-button\">I've got it!</div>\n" +
//                "            </li>\n" +
//                "        </#list>";
        List<Purchase> products = purchaseService.nonDelivered(authUser.getNickname());
        String result = "";
        for(Purchase product : products){
            result = result+"<li class=\"row\">"+product.getSubscription()+
                "\n<div id=\"del_"+product.getId()+"_\" class=\"but delivered-button\">I've got it!</div>"+
            "\n</li>\n";
        }
        System.out.println(result);
        return result;
    }

    @RequestMapping(value = "/find/{mask}", method = RequestMethod.GET)
    @ResponseBody
    public String findByMask(@PathVariable("mask") String mask, Authentication authentication){
        System.out.println("AjaxController findByMask func");
        mask = mask.toLowerCase();
        //CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        List<Product> products = productService.findByMask(mask);
        String result = "";
        for(Product product : products){
            result = result + "<a href=\"/product/"+product.getId()+"\">\n" +
                    "    <div class=\"row\">\n" +
                    "        <div class=\"cell\"><img src=\""+product.getPhotoPath()+"\" class=\"logo-small\"></div>" +
                    "        <div class=\"cell\">"+product.getName()+"</div>\n" +
                    "        <div class=\"cell\">"+product.getShop()+"</div>\n" +
                    "        <div class=\"cell\">"+product.getPrice()+"</div>\n" +
                    "        <div class=\"cell\">"+product.getSold()+"</div>\n" +
                    "    </div>\n" +
                    "    </a>";
        }
        //System.out.println(result);
        return result;
    }

    @RequestMapping(value = "addShop/{nickname}", method = RequestMethod.GET)
    @ResponseBody
    public String verifyAddShop(@PathVariable("nickname") String nickname){
        System.out.println("verifyAddShop ajaxController");
        List<Purchase> undelivered = purchaseService.nonDelivered(nickname);
        String result = new String();
        System.out.println(undelivered.toString());
        if(undelivered.isEmpty()){
            System.out.println("verifyAddShop ajaxController empty undelivered list");
            result = "OK";
        }
        else result = "!OK";
        return result;
    }

}
