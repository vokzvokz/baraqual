package com.baraqual.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public ModelAndView login(@RequestParam(value = "error", required = false) String error){
//
//        ModelAndView model = new ModelAndView();
//        if(error != null){
//            model.addObject("error", "The username or password is incorrect!");
//        }
//
//        model.setViewName("login");
//
//        return model;
//    }

    @GetMapping("/login")
    public String login(Model model,  String error) {
        if (error != null)
            model.addAttribute("error", "Your username or password is not correct!");
        return "login";
    }

//    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
//    public ModelAndView home(){
//        ModelAndView model = new ModelAndView();
//        model.setViewName("home");
//        return model;
//    }
}