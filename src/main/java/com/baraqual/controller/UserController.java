package com.baraqual.controller;

import com.baraqual.config.init.CustomUserDetails;
import com.baraqual.entity.Product;
import com.baraqual.entity.Shop;
import com.baraqual.entity.User;
import com.baraqual.service.ProductService;
import com.baraqual.service.PurchaseService;
import com.baraqual.service.UserService;
import com.baraqual.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.MultipartConfigElement;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class UserController {

    @Qualifier("userServiceImpl")
    @Autowired
    public UserService userService;

    @Autowired
    public ShopService shopService;

    @Autowired
    public ProductService productService;

    @Autowired
    public PurchaseService purchaseService;

    @Autowired
    public MultipartConfigElement multipartConfigElement;

    @Autowired
    public MultipartResolver multipartResolver;

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @GetMapping("/users")
    public String getAllUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "usersList";
    }

    @GetMapping("/shops")
    public String getAllShops(Model model) {
        model.addAttribute("shops", shopService.findAll());
        return "shopsList";
    }

    @GetMapping("/products")
    public String getAllPProducts(Model model) {
        model.addAttribute("products", productService.findAll());
        return "productsList";
    }

    @GetMapping("/user/{nickname}")
    public String getUserByNickname(@PathVariable("nickname") String nickname, Model model) {
        model.addAttribute("purchases", purchaseService.findByUser(nickname));
        model.addAttribute("user", userService.getByNickname(nickname));
        return "showUser";
    }

    @GetMapping("/shop/{owner}")
    public String getShopByOwner(@PathVariable("owner") String owner, Model model, Authentication authentication) {
        model.addAttribute("shop", shopService.getByOwner(owner));
        model.addAttribute("products", shopService.findAllProducts(owner));
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        if(authUser.getRole().equals("ROLE_ADMIN")) return "showShopAdmin";
        return "showShop";
    }

    @GetMapping("/addUser")
    public String createUserPage() {
        return "createUser";
    }

    @PostMapping("/addUser")
    public String addUser(@ModelAttribute("user") User user) {
        userService.save(user);
        return "redirect:/shops";
    }

    @PreAuthorize("hasAnyRole('ROLE_USER')")
    @GetMapping("/addShop")
    public String createNewShop() {
        System.out.println("createNewShop UserController");
        return "createShop";
    }

    @PostMapping(value = "/addShop", consumes = "multipart/form-data")
    public String addShop(@ModelAttribute("shop") Shop shop, Authentication authentication, @RequestParam("file") MultipartFile image) {
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        shop.setOwner(authUser.getUsername());
        if (image != null) {
            String location = "C:\\Users\\Vladimir\\IdeaProjects\\baraqual\\src\\main\\webapp\\WEB-INF\\views\\pics\\";
            String fileName = image.getOriginalFilename();
            fileName = fileName.substring(fileName.lastIndexOf('.'), fileName.length());
            System.out.println(fileName);
            fileName = "logo_" + authUser.getUsername() + fileName;
            File pathFile = new File(location);
            if (!pathFile.exists()) {
                pathFile.mkdir();
            }
            pathFile = new File(location + fileName);
            try {
                image.transferTo(pathFile);
                //result = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            shop.setLogoPath("/img/" + fileName);
        } else {
            shop.setLogoPath("/img/empty.svg");
        }
        shopService.save(shop);
        userService.setRole("ROLE_SHOP", authUser.getNickname());
        authentication.setAuthenticated(false);
        return "redirect:/index";
    }
    @GetMapping("/update/{nickname}")
    public String updateUser(@PathVariable("nickname") String nickname, Model model) {
        model.addAttribute("user", userService.getByNickname(nickname));
        return "editUser";
    }

    @GetMapping("/delete/{nickname}")
    public String deleteUser(@PathVariable("nickname") String nickname) {
        userService.delete(nickname);
        return "redirect:/users";
    }

    @PostMapping("/updateUser")
    public String updateUser(@ModelAttribute("user") User user) {
        userService.update(user);
        return "redirect:/user/" + user.getNickname();
    }

    @PreAuthorize("hasAnyRole('ROLE_USER')")
    @GetMapping("/homepage")
    public String userHome(Authentication authentication, Model model) {
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        model.addAttribute("user", authUser);
        model.addAttribute("nonDeliveredProducts", purchaseService.nonDelivered(authUser.getNickname()));
        return "homepage";
    }

    @PreAuthorize("hasAnyRole('ROLE_SHOP')")
    @GetMapping("/shopHome")
    public String shopHome(Authentication authentication, Model model) {
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        model.addAttribute("user", authUser);
        model.addAttribute("shop", shopService.getByOwner(authUser.getNickname()));
        model.addAttribute("products", shopService.findAllProducts(authUser.getUsername()));
        return "shopHome";
    }


    @PreAuthorize("hasAnyRole('ROLE_SHOP')")
    @GetMapping("/addProduct")
    public String createProduct(){
        System.out.println("createProduct UserService");
        return "createProduct";
    }

    @PostMapping("/addProduct")
    public String addProduct(Authentication authentication, @ModelAttribute("product") Product product,
                             @RequestParam("file") MultipartFile image){
        product.setShop(authentication.getName());
        CustomUserDetails authUser = (CustomUserDetails) (authentication.getPrincipal());
        //
        if (image != null) {
            String location = "C:\\Users\\Vladimir\\IdeaProjects\\baraqual\\src\\main\\webapp\\WEB-INF\\views\\pics\\";
            String fileName = image.getOriginalFilename();
            fileName = fileName.substring(fileName.lastIndexOf('.'), fileName.length());
            System.out.println(fileName);
            fileName = "photo_" + product.getShop() +"_" + product.getName() + fileName;
            File pathFile = new File(location);
            if (!pathFile.exists()) {
                pathFile.mkdir();
            }
            pathFile = new File(location + fileName);
            try {
                image.transferTo(pathFile);
                //result = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            product.setPhotoPath("/img/" + fileName);
        } else {
            product.setPhotoPath("/img/empty.svg");
        }
        System.out.println(product.getPhotoPath());
        productService.save(product);
        return "redirect:/shopHome";
    }

    @GetMapping("/product/{id}")
    public String getProductById(@PathVariable("id") int id, Model model){
        model.addAttribute("product", productService.getById(id));
        return "showProduct";
    }

    @GetMapping("/approve/{id}")
    public String changeProve(@PathVariable("id") int id){
        Shop shop = shopService.getById(id);
        shop.setApproved(!shop.isApproved());
        shopService.update(shop);
        return "redirect:/users";
    }

    @GetMapping("/exit")
    public String exit(Authentication authentication){
        authentication.setAuthenticated(false);
        return "redirect:/index";
    }

}
