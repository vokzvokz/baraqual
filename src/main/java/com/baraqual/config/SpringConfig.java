package com.baraqual.config;

import com.baraqual.controller.UserController;
import com.baraqual.dao.*;
import com.baraqual.entity.Product;
import com.baraqual.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = {"com.baraqual.service", "com.baraqual.dao"})
public class SpringConfig {

    @Bean
    public JdbcTemplate getJdbcTemplate(){
        return new JdbcTemplate(getDataSource());
    }

    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres?useSll=false");
        dataSource.setUsername("postgres");
        dataSource.setPassword("112358132134");
        dataSource.setDriverClassName("org.postgresql.Driver");
        return dataSource;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return charSequence.toString();
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return charSequence.toString().equals(s);
            }
        };
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }

    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(100000000);
        return multipartResolver;
    }

    @Bean
    public UserDao getUserDao(){
        return new UserDaoImpl(getJdbcTemplate());
    }

    @Bean
    public UserService getUserService(){
        return  new UserServiceImpl();
    }

    @Bean
    public ShopDao getShopDao(){
        return new ShopDaoImpl(getJdbcTemplate());
    }

    @Bean
    public ShopService getShopService(){
        return new ShopServiceImpl();
    }

    @Bean
    public ProductDao getProductDao(){return new ProductDaoImpl(getJdbcTemplate());}

    @Bean
    public ProductService getProductService(){return new ProductServiceImpl();}

    @Bean
    public PurchaseDao getPurchaseDao(){return new PurchaseDaoImpl(getJdbcTemplate());}

    @Bean
    public PurchaseService getPurchaseService(){return new PurchaseServiceImpl();}

//    @Bean
//    UserController getUserController(){return new UserController();}



//    @Bean
//    public UserDetailsService getUserDetailsService(){
//        return new UserDetailsServiceImpl();
//    }


}
