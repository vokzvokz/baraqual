package com.baraqual.config.init;

import com.baraqual.dao.UserDao;
import com.baraqual.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//@Component
public class CustomUserDetails extends User implements UserDetails {

    private UserDao loginDao;

    public CustomUserDetails(final User user, UserDao _loginDao) {
        super(user);
        loginDao = _loginDao;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<String> roles = loginDao.getUserRoles(getUsername());

        List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();

        if (roles != null) {
            for (String role : roles) {
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantedAuthorityList.add(authority);
            }
        }
        return grantedAuthorityList;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}