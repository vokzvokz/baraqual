package com.baraqual.config;

//import com.baraqual.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.baraqual.controller")
//@Import({SecurityConfig.class, SpringConfig.class})
public class WebConfig extends WebMvcConfigurerAdapter {
//
//    @Autowired
//    DataSource dataSource;
//
//    @Bean
//    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
//        return new NamedParameterJdbcTemplate(dataSource);
//    }



    @Bean
    public ViewResolver getViewResolver(){
        FreeMarkerViewResolver freeMarkerViewResolver = new FreeMarkerViewResolver();
        freeMarkerViewResolver.setOrder(1);
        freeMarkerViewResolver.setSuffix(".ftl");
        freeMarkerViewResolver.setPrefix("");
        freeMarkerViewResolver.setContentType("text/html; charset=UTF-8");
        return freeMarkerViewResolver;
    }
    @Bean
    public FreeMarkerConfigurer getFreeMarkerConfigurer(){
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPaths("/", "/WEB-INF/views/");
        freeMarkerConfigurer.setDefaultEncoding("UTF-8");
        return freeMarkerConfigurer;
    }

    @Bean
    public UrlBasedViewResolver setupViewResolver() {
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".ftl");
        resolver.setViewClass(JstlView.class);

        return resolver;
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        registry.addResourceHandler("/style/**").addResourceLocations("file:/"+
                "C:\\Users\\Vladimir\\IdeaProjects\\baraqual\\src\\main\\webapp\\WEB-INF\\views\\css"+"/");
        registry.addResourceHandler("/script/**").addResourceLocations("file:/"+
                "C:\\Users\\Vladimir\\IdeaProjects\\baraqual\\src\\main\\webapp\\WEB-INF\\views\\js"+"/");
        registry.addResourceHandler("/img/**").addResourceLocations("file:/"+
                "C:\\Users\\Vladimir\\IdeaProjects\\baraqual\\src\\main\\webapp\\WEB-INF\\views\\pics"+"/");
    }

}
