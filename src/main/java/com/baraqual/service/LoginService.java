package com.baraqual.service;

import com.baraqual.config.init.CustomUserDetails;
import com.baraqual.dao.ShopDao;
import com.baraqual.dao.UserDao;
import com.baraqual.entity.Shop;
import com.baraqual.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LoginService implements UserDetailsService {
    @Qualifier("userDaoImpl")
    @Autowired
    UserDao loginDao;

//    @Autowired
//    ShopDao loginDaoShop;

    public void setLoginDao(UserDao loginDao) {
        this.loginDao = loginDao;
    }

//    public void setLoginDaoShop(ShopDao loginDaoShop){
//        this.loginDaoShop = loginDaoShop;
//    }

    //    @Override
//    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
//        User userInfo = loginDao.getByUsername(userName);
//        if(userInfo==null){
//            throw new UsernameNotFoundException("user name not found");
//        }
//        List<String> roles = loginDao.getUserRoles(userName);
//
//        List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();
//
//        if(roles!=null){
//            for(String role : roles){
//                GrantedAuthority authority = new SimpleGrantedAuthority(role);
//                grantedAuthorityList.add(authority);
//            }
//        }
//        UserDetails userDetails = new org.springframework.security.core.userdetails.User(userInfo.getUsername(), userInfo.getPassword(), grantedAuthorityList);
//        return userDetails;
//    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User userInfo = loginDao.getByNickname(username);
//        Shop shopInfo = new Shop();
//        if(userInfo == null){
//            shopInfo = loginDaoShop.getByNickname(username);
//        }
        if(userInfo==null){
            throw new UsernameNotFoundException("username not found");
        }

        List<String> roles = loginDao.getUserRoles(username);

        List<GrantedAuthority> grantedAuthorityList = new ArrayList<GrantedAuthority>();

        if(roles!=null){
            for(String role : roles){
                GrantedAuthority authority = new SimpleGrantedAuthority(role);
                grantedAuthorityList.add(authority);
            }
        }
        CustomUserDetails customUserDetails=new CustomUserDetails(userInfo,loginDao);
        // UserDetails userDetails = new org.springframework.security.core.userdetails.User(userInfo.getUsername(), userInfo.getPassword(), grantedAuthorityList);
        return customUserDetails;

    }
}
