package com.baraqual.service;

import com.baraqual.dao.UserDao;
import com.baraqual.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Qualifier("getUserDao")
    @Autowired
    public UserDao userDao;

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public List<String> getUserRoles(String nickname) {
        return userDao.getUserRoles(nickname);
    }

    @Override
    public void setRole(String role, String nickname){
        userDao.setRole(role, nickname);
    };

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public User getByNickname(String nickname) {
        return userDao.getByNickname(nickname);
    }

    @Override
    public void delete(String nickname) {
        userDao.delete(nickname);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }
}
