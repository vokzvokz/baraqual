package com.baraqual.service;

import com.baraqual.dao.ShopDao;
import com.baraqual.entity.Product;
import com.baraqual.entity.Shop;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ShopServiceImpl implements ShopService {
    @Autowired
    public ShopDao shopDao;

    @Override
    public List<Shop> findAll(){
        return shopDao.findAll();
    }

    @Override
    public List<Product> findAllProducts(String nickname) {
        return shopDao.findAllProducts(nickname);
    }

    @Override
    public void save(Shop shop) {
        shopDao.save(shop);
    }

    @Override
    public Shop getByOwner(String owner) {
        return shopDao.getByOwner(owner);
    }

    @Override
    public Shop getById(int id) {
        return shopDao.getById(id);
    }

    @Override
    public void recalculateSold(String owner) {
        List<Product> products = shopDao.findAllProducts(owner);
        Shop shop = shopDao.getByOwner(owner);
        int totalSold = 0;
        for(Product product : products){
            totalSold+=product.getSold();
        }
        shop.setSold(totalSold);
        shopDao.update(shop);
    }

    @Override
    public void delete(String owner) {
        shopDao.delete(owner);
    }

    @Override
    public void update(Shop shop) {
        shopDao.update(shop);
    }
}
