package com.baraqual.service;

import com.baraqual.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();
    List<Product> findByName(String name);
    List<Product> findByMask(String mask);
    void save(Product product);
    Product getById(int id);
    void purchased(int id);
    void delete(int id);
    void update(Product product);
}
