package com.baraqual.service;

import com.baraqual.dao.ProductDao;
import com.baraqual.entity.Product;
import com.baraqual.entity.Shop;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    @Autowired
    public ProductDao productDao;

    @Autowired
    public ShopService shopService;

    @Override
    public List<Product> findAll() {
        return productDao.findAll();
    }

    @Override
    public List<Product> findByName(String name) {
        return productDao.findByName(name);
    }

    @Override
    public List<Product> findByMask(String mask) {
        return productDao.findByMask(mask);
    }

    @Override
    public void save(Product product) {
        productDao.save(product);
    }

    @Override
    public Product getById(int id) {
        return productDao.getById(id);
    }

    @Override
    public void purchased(int id) {
        productDao.purchased(id);
        Product product = productDao.getById(id);
        product.purchased();
        shopService.recalculateSold(product.shop);
    }

    @Override
    public void delete(int id) {
        productDao.delete(id);
    }

    @Override
    public void update(Product product) {
        productDao.update(product);
    }
}
