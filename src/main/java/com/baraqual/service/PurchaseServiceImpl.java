package com.baraqual.service;

import com.baraqual.dao.PurchaseDao;
import com.baraqual.entity.Purchase;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PurchaseServiceImpl implements PurchaseService {

    @Autowired
    PurchaseDao purchaseDao;

    @Autowired
    ProductService productService;

    @Override
    public void createPurchase(String user, int product) {
        purchaseDao.createPurchase(user, product);
        productService.purchased(product);
    }

    @Override
    public List<Purchase> nonDelivered(String user) {
        return purchaseDao.nonDelivered(user);
    }

    @Override
    public List<Purchase> findByProduct(int product) {
        return purchaseDao.findByProduct(product);
    }

    @Override
    public List<Purchase> findByUser(String user) {
        return purchaseDao.findByUser(user);
    }

    @Override
    public List<Purchase> findAll() {
        return purchaseDao.findAll();
    }

    @Override
    public Purchase findById(int id) {
        return purchaseDao.findById(id);
    }

    @Override
    public void delivered(int id) {
        purchaseDao.delivered(id);
    }
}
